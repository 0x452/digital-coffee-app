package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.digital.coffee.repository.UserRepository
import com.digital.coffee.utils.getErrorResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    private var userRepository = UserRepository()

    private var _eventNetworkError = MutableLiveData(false)
    private var _isNetworkErrorShown = MutableLiveData(false)
    private var _networkMessage = MutableLiveData(String())

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown
    val networkMessage: LiveData<String>
        get() = _networkMessage

    val auth = userRepository.authData

    fun authorize(username: String, phone: String) {
        viewModelScope.launch {
            try {
                userRepository.authorize(username, phone)
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false
            } catch (networkError: HttpException) {
                _eventNetworkError.value = true
                _networkMessage.value = networkError.getErrorResponse()?.message
            }
        }
    }

    class Factory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return LoginViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}