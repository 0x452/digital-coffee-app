package com.digital.coffee.viewmodels

import androidx.lifecycle.ViewModel
import com.digital.coffee.network.data.Condiment
import com.digital.coffee.utils.CartManager

class SharedViewModel: ViewModel() {
    private var cartManager: CartManager = CartManager()
    private var condimentsList: List<Condiment> = listOf()

    fun getCartManager(): CartManager = cartManager

    fun setCondimentsList(condiments: List<Condiment>) {
        condimentsList = condiments
    }

    fun getCondiments(): List<Condiment> = condimentsList
}