package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.digital.coffee.network.data.ErrorResponse
import com.digital.coffee.repository.CoffeeshopRepository
import com.digital.coffee.utils.getErrorResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException

class MapViewModel (application: Application) : AndroidViewModel(application) {
    private var coffeeshopRepository = CoffeeshopRepository()

    private var _eventNetworkError = MutableLiveData(false)
    private var _isNetworkErrorShown = MutableLiveData(false)
    private var _networkMessage = MutableLiveData(ErrorResponse("", 0))

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown
    val network: LiveData<ErrorResponse>
        get() = _networkMessage

    val markersData = coffeeshopRepository.markers

    init {
        getDataFromRepository()
    }

    private fun getDataFromRepository() {
        viewModelScope.launch {
            try {
                coffeeshopRepository.getCoffeeshopsMarkers()
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false
            } catch (networkError: HttpException) {
                _networkMessage.value = networkError.getErrorResponse()
                _eventNetworkError.value = true
            }
        }
    }

    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MapViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MapViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}