package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.digital.coffee.network.data.CartItem
import com.digital.coffee.network.data.Condiment

class DetailsViewModel(application: Application): AndroidViewModel(application) {

    private var condiments: MutableList<Condiment> = mutableListOf()
    private lateinit var detailsItem: CartItem

    fun setDetailsItem(item: CartItem) {
        detailsItem = item
    }

    fun getDetailsItem(): CartItem = detailsItem

    fun getItemTotalPrice(): Int {
        var price = 0
        detailsItem.condiments.forEach {
            price += it.price * it.amount
        }
        return detailsItem.price.times(detailsItem.amount) + price
    }

    fun addCondimentInItem(it: Condiment): Int {
        var amount = 1
        val index = condiments.indexOf(it)
        if(condiments.singleOrNull { condiment -> condiment.title == it.title } != null) {
            condiments[index].amount += 1
            amount = condiments[index].amount
        } else {
            condiments.add(Condiment(it.title, it.price, it.url, 1))
        }

        detailsItem.condiments = condiments

        return amount
    }


    class Factory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return DetailsViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}