package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.digital.coffee.network.data.CoffeeshopOrder
import com.digital.coffee.repository.CoffeeshopRepository
import com.digital.coffee.utils.getErrorResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException

class CheckoutViewModel(application: Application): AndroidViewModel(application) {
    private var coffeeshopRepository = CoffeeshopRepository()

    private var _eventNetworkError = MutableLiveData(false)
    private var _isNetworkErrorShown = MutableLiveData(false)
    private var _networkMessage = MutableLiveData(String())

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown
    val networkMessage: LiveData<String>
        get() = _networkMessage

    val order = coffeeshopRepository.orderData

    fun sendOrderToCoffeeshop(coffeeshopId: Int, order: CoffeeshopOrder) {
        viewModelScope.launch {
            try {
                coffeeshopRepository.sendToCoffeeshop(coffeeshopId, order)
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false
            } catch (networkError: HttpException) {
                _networkMessage.value = networkError.getErrorResponse()?.message
                _eventNetworkError.value = true
            }
        }
    }

    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CheckoutViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return CheckoutViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}