package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.digital.coffee.network.data.ErrorResponse
import com.digital.coffee.repository.CoffeeshopRepository
import com.digital.coffee.utils.getErrorResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException

class MenuViewModel (application: Application, coffeeshopId: Int) : AndroidViewModel(application) {
    private var coffeeshopRepository = CoffeeshopRepository()

    private var _eventNetworkError = MutableLiveData(false)
    private var _isNetworkErrorShown = MutableLiveData(false)
    private var _networkMessage = MutableLiveData(ErrorResponse("", 0))

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown
    val network: LiveData<ErrorResponse>
        get() = _networkMessage

    val coffeeshopData = coffeeshopRepository.coffeeshop

    init {
        getDataFromRepository(coffeeshopId)
    }

    private fun getDataFromRepository(coffeeshopId: Int) {
        viewModelScope.launch {
            try {
                coffeeshopRepository.getCoffeeshop(coffeeshopId)
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false
            } catch (networkError: HttpException) {
                _networkMessage.value = networkError.getErrorResponse()
                _eventNetworkError.value = true
            }
        }
    }

    class Factory(val app: Application, val coffeeshopId: Int) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MenuViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MenuViewModel(app, coffeeshopId) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}