package com.digital.coffee.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.digital.coffee.repository.CoffeeshopRepository
import com.digital.coffee.utils.getErrorResponse
import kotlinx.coroutines.launch
import retrofit2.HttpException

class StatusViewModel(application: Application) : AndroidViewModel(application) {
    private var coffeeshopRepository = CoffeeshopRepository()

    private var _eventNetworkError = MutableLiveData(false)
    private var _isNetworkErrorShown = MutableLiveData(false)
    private var _networkMessage = MutableLiveData(String())

    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown
    val networkMessage: LiveData<String>
        get() = _networkMessage

    val status = coffeeshopRepository.status

    fun getCoffeeshopStatus(coffeeshopId: Int, orderId: Int) {
        viewModelScope.launch {
            try {
                coffeeshopRepository.getOrderStatus(coffeeshopId, orderId)
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false
            } catch (networkError: HttpException) {
                _eventNetworkError.value = true
                _networkMessage.value = networkError.getErrorResponse()?.message
            }
        }
    }

    class Factory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(StatusViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return StatusViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}