package com.digital.coffee

import android.app.Application
import android.content.Context
import timber.log.Timber

class DigitalCoffeeApp : Application() {

    companion object {
        private lateinit var context: Context
        fun getContext(): Context = context
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        context = applicationContext
    }
}