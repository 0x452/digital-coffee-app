package com.digital.coffee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.databinding.CondimentItemBinding
import com.digital.coffee.network.data.CoffeeshopItem
import com.digital.coffee.network.data.Condiment
import timber.log.Timber

class CondimentsAdapter(val callback: CondimentClick): RecyclerView.Adapter<CondimentsViewHolder>() {
    var data: List<Condiment> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private lateinit var withDataBinding: CondimentItemBinding
    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CondimentsViewHolder {
        withDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            CondimentsViewHolder.LAYOUT,
            parent,
            false)

        return CondimentsViewHolder(withDataBinding)
    }

    override fun onBindViewHolder(holder: CondimentsViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.item = data[position]
            it.position = position
            it.callback = callback
        }
    }

    fun setCondimentAmount(position: Int, amount: Int) {
        data[position].amount = amount
        notifyDataSetChanged()
    }
}

class CondimentsViewHolder(val viewDataBinding: CondimentItemBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.condiment_item
    }
}

class CondimentClick(val block: (Condiment, Int) -> Unit) {
    fun onClick(item: Condiment, position: Int) = block(item, position)
}