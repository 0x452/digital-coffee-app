package com.digital.coffee.adapters

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.digital.coffee.R
import com.digital.coffee.utils.map.MapClusterItem
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer


class ClusterAdapter(
    private var context: Context,
    map: GoogleMap,
    clusterManager: ClusterManager<MapClusterItem>
) : DefaultClusterRenderer<MapClusterItem>(context, map, clusterManager) {

    override fun onClusterItemRendered(clusterItem: MapClusterItem, marker: Marker) {
        super.onClusterItemRendered(clusterItem, marker)

        marker.isVisible = false

        Glide.with(context)
            .asBitmap()
            .load(clusterItem.getImageUrl())
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val bitmap = drawMarkerWithImage(resource)
                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap))
                    marker.isVisible = true
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    TODO("Not yet implemented")
                }
            })
    }

    private fun drawMarkerWithImage(image: Bitmap): Bitmap {
        val bitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.ARGB_8888)
        val rect = RectF(0.0f, 0.0f, 80.0f, 80.0f)

        val clipPath = Path()
        val paint = Paint()

        clipPath.addRoundRect(rect, 80.0f, 80.0f, Path.Direction.CW)

        paint.color = ContextCompat.getColor(context, R.color.colorGlobal);
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 11.0f
        paint.isAntiAlias = true

        val canvas = Canvas(bitmap)

        canvas.clipPath(clipPath)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(
            Bitmap.createScaledBitmap(image, 62, 62, false),
            8.5f, 8.5f, null
        )
        canvas.drawRoundRect(rect, 80.0f, 80.0f, paint)

        return bitmap
    }
}