package com.digital.coffee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.databinding.MenuItemBinding
import com.digital.coffee.network.data.CoffeeshopItem

class MenuAdapter(val callback: ItemClick) : RecyclerView.Adapter<MenuViewHolder>() {
    var data: List<CoffeeshopItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val withDataBinding: MenuItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            MenuViewHolder.LAYOUT,
            parent,
            false)

        return MenuViewHolder(withDataBinding)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.item = data[position]
            it.callback = callback
        }
    }

}

class MenuViewHolder(val viewDataBinding: MenuItemBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.menu_item
    }
}


class ItemClick(val block: (CoffeeshopItem) -> Unit) {
    fun onClick(item: CoffeeshopItem) = block(item)
}
