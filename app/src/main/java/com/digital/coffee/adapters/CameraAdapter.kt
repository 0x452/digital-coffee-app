package com.digital.coffee.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.SparseArray
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.core.util.isNotEmpty
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.digital.coffee.utils.Event
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import timber.log.Timber
import java.lang.Exception

class CameraAdapter(context: Context, cameraView: SurfaceView) {
    private var detector: BarcodeDetector = BarcodeDetector.Builder(context).build()
    private var cameraSource: CameraSource = CameraSource.Builder(context, detector)
        .setAutoFocusEnabled(true)
        .build()

    private val _navigateToMenu = MutableLiveData<Event<Int>>()
    val navigateToMenu : LiveData<Event<Int>>
        get() = _navigateToMenu

    private val surfaceCallback = object : SurfaceHolder.Callback {
        override fun surfaceChanged(p0: SurfaceHolder, p1: Int, p2: Int, p3: Int) { }

        @SuppressLint("MissingPermission")
        override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
            try {
                cameraSource.start(surfaceHolder)
            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }

        override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
            cameraSource.stop()
        }
    }

    private val processor = object : Detector.Processor<Barcode> {
        override fun release() { }

        override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
            if (detections != null && detections.detectedItems.isNotEmpty()) {
                val qrCodes: SparseArray<Barcode> = detections.detectedItems
                _navigateToMenu.postValue(Event(qrCodes.valueAt(0).rawValue.toInt()))
            }
        }
    }

    init {
        cameraView.holder.addCallback(surfaceCallback)
        detector.setProcessor(processor)
    }
}