package com.digital.coffee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.databinding.ProfileItemBinding
import com.digital.coffee.network.data.LatestOrdersData

class ProfileAdapter: RecyclerView.Adapter<ProfileViewHolder>() {
    var data: List<LatestOrdersData> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
        val withDataBinding: ProfileItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            ProfileViewHolder.LAYOUT,
            parent,
            false)

        return ProfileViewHolder(withDataBinding)
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        holder.viewDataBinding.also {
            it.item = data[position]
        }
    }

    override fun getItemCount(): Int = data.size
}

class ProfileViewHolder(val viewDataBinding: ProfileItemBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.profile_item
    }
}