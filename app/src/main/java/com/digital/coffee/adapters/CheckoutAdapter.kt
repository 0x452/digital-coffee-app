package com.digital.coffee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.databinding.CheckoutItemBinding
import com.digital.coffee.network.data.CartItem

class CheckoutAdapter : RecyclerView.Adapter<CheckoutViewHolder>() {
    var data: MutableList<CartItem> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val withDataBinding: CheckoutItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            CheckoutViewHolder.LAYOUT,
            parent,
            false)

        return CheckoutViewHolder(withDataBinding)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        holder.viewDataBinding.also { view ->
            view.item = data[position]

            view.minus.setOnClickListener {
                data[position].amount--
                if(data[position].amount == 0) {
                    data.removeAt(position)
                    notifyDataSetChanged()
                } else {
                    view.item = data[position]
                }
            }

            view.plus.setOnClickListener {
                data[position].amount++
                view.item = data[position]
            }
        }
    }

    fun removeAt(index: Int) {
        data.removeAt(index)
        notifyDataSetChanged()
    }
}

class CheckoutViewHolder(val viewDataBinding: CheckoutItemBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    companion object {
        @LayoutRes
        val LAYOUT = R.layout.checkout_item
    }
}