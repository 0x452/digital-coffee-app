package com.digital.coffee.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPref(context: Context) {
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences("data", Context.MODE_PRIVATE)

    fun pushToken(token: String) = sharedPreferences.edit().putString("fcm_token", token).apply()
    fun getToken(): String = sharedPreferences.getString("fcm_token", "").toString()

    fun pushUserToken(token: String) = sharedPreferences.edit().putString("token", token).apply()
    fun getUserToken(): String = sharedPreferences.getString("token", "").toString()
}