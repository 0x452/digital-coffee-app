package com.digital.coffee.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String) {
    Glide.with(imageView.context).load(url).into(imageView)
}

@BindingAdapter("convertedTimestamp")
fun convertTimestamp(textView: TextView, date: String) {
    textView.text = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault()).format(
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SSS", Locale.getDefault()).parse(date)!!
    ).toString()
}