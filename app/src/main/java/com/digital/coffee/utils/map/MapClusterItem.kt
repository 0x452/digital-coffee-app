package com.digital.coffee.utils.map

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
class MapClusterItem(
    private val id: Int,
    var lat: Double,
    var lng: Double,
    private val title: String,
    private val snippet: String,
    private val url: String
) : ClusterItem, Parcelable {

    @IgnoredOnParcel private val position: LatLng = LatLng(lat, lng)

    override fun getSnippet(): String = snippet
    override fun getPosition(): LatLng = position
    override fun getTitle(): String = title
    fun getId(): Int = id
    fun getImageUrl(): String = url
}