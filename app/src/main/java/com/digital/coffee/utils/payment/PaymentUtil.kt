package com.digital.coffee.utils.payment

import android.app.Activity
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.gms.wallet.Wallet
import com.google.android.gms.wallet.WalletConstants
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

object PaymentsUtil {
    object Constants {
        const val LOAD_PAYMENT_DATA_REQUEST_CODE = 991

        const val PAYMENTS_ENVIRONMENT = WalletConstants.ENVIRONMENT_TEST

        val SUPPORTED_NETWORKS = listOf(
            "MASTERCARD",
            "VISA")

        val SUPPORTED_METHODS = listOf(
            "PAN_ONLY",
            "CRYPTOGRAM_3DS")

        const val COUNTRY_CODE = "RU"
        const val CURRENCY_CODE = "RUB"

        private const val PAYMENT_GATEWAY_TOKENIZATION_NAME = "example"

        val PAYMENT_GATEWAY_TOKENIZATION_PARAMETERS = mapOf(
            "gateway" to PAYMENT_GATEWAY_TOKENIZATION_NAME,
            "gatewayMerchantId" to "exampleGatewayMerchantId"
        )
    }
    private val baseRequest = JSONObject().apply {
        put("apiVersion", 2)
        put("apiVersionMinor", 0)
    }

    private fun gatewayTokenizationSpecification(): JSONObject {
        return JSONObject().apply {
            put("type", "PAYMENT_GATEWAY")
            put("parameters", JSONObject(Constants.PAYMENT_GATEWAY_TOKENIZATION_PARAMETERS))
        }
    }

    private val allowedCardNetworks = JSONArray(Constants.SUPPORTED_NETWORKS)
    private val allowedCardAuthMethods = JSONArray(Constants.SUPPORTED_METHODS)

    private fun baseCardPaymentMethod(): JSONObject {
        return JSONObject().apply {

            val parameters = JSONObject().apply {
                put("allowedAuthMethods",
                    allowedCardAuthMethods
                )
                put("allowedCardNetworks",
                    allowedCardNetworks
                )
                put("billingAddressRequired", false)
                put("billingAddressParameters", JSONObject().apply {
                    put("format", "FULL")
                })
            }

            put("type", "CARD")
            put("parameters", parameters)
        }
    }

    private fun cardPaymentMethod(): JSONObject {
        val cardPaymentMethod =
            baseCardPaymentMethod()
        cardPaymentMethod.put("tokenizationSpecification",
            gatewayTokenizationSpecification()
        )

        return cardPaymentMethod
    }

    fun isReadyToPayRequest(): JSONObject? {
        return try {
            val isReadyToPayRequest = JSONObject(baseRequest.toString())
            isReadyToPayRequest.put(
                "allowedPaymentMethods", JSONArray().put(baseCardPaymentMethod()))

            isReadyToPayRequest

        } catch (e: JSONException) {
            null
        }
    }

    private val merchantInfo: JSONObject
        @Throws(JSONException::class)
        get() = JSONObject().put("merchantName", "Example Merchant")

    fun createPaymentsClient(activity: Activity): PaymentsClient {
        val walletOptions = Wallet.WalletOptions.Builder()
            .setEnvironment(Constants.PAYMENTS_ENVIRONMENT)
            .build()

        return Wallet.getPaymentsClient(activity, walletOptions)
    }

    @Throws(JSONException::class)
    private fun getTransactionInfo(price: String): JSONObject {
        return JSONObject().apply {
            put("totalPrice", price)
            put("totalPriceStatus", "FINAL")
            put("countryCode",
                Constants.COUNTRY_CODE
            )
            put("currencyCode",
                Constants.CURRENCY_CODE
            )
        }
    }

    fun getPaymentDataRequest(price: String): JSONObject? {
        return try {
            JSONObject(baseRequest.toString()).apply {
                put("allowedPaymentMethods", JSONArray().put(cardPaymentMethod()))
                put("transactionInfo",
                    getTransactionInfo(
                        price
                    )
                )
                put("merchantInfo",
                    merchantInfo
                )
            }
        } catch (e: JSONException) {
            null
        }

    }
}