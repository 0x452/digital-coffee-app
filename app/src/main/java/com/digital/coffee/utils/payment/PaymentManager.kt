package com.digital.coffee.utils.payment

import android.app.Activity
import android.widget.Toast
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.AutoResolveHelper
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentDataRequest
import com.google.android.gms.wallet.PaymentsClient
import timber.log.Timber

class PaymentManager(private val activity: Activity) {
    private var paymentsClient: PaymentsClient = PaymentsUtil.createPaymentsClient(activity)

    fun possiblyShowGooglePayButton() {
        val isReadyToPayJson = PaymentsUtil.isReadyToPayRequest()
        val request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString())

        val task = paymentsClient.isReadyToPay(request)
        task.addOnCompleteListener { completedTask ->
            try {
                completedTask.getResult(ApiException::class.java)?.let(::setGooglePayAvailable)
            } catch (exception: ApiException) {
                Timber.d("isReadyToPay failed")
            }
        }
    }

    private fun setGooglePayAvailable(available: Boolean) {
        if (!available) {
            Toast.makeText(activity.applicationContext,
                "Unfortunately, Google Pay is not available on this device",
                Toast.LENGTH_LONG).show();
        }
    }

    fun processPayment(cost: Int) {
        val paymentDataRequestJson = PaymentsUtil.getPaymentDataRequest(cost.toString())
        if (paymentDataRequestJson == null) {
            Timber.d("Can't fetch payment data request")
            return
        }
        val request = PaymentDataRequest.fromJson(paymentDataRequestJson.toString())

        if (request != null) {
            AutoResolveHelper.resolveTask(
                paymentsClient.loadPaymentData(request), activity, PaymentsUtil.Constants.LOAD_PAYMENT_DATA_REQUEST_CODE)
        }
    }
}