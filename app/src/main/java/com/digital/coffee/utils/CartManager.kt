package com.digital.coffee.utils

import com.digital.coffee.network.data.CartItem
import timber.log.Timber

class CartManager {
    private var items : MutableList<CartItem> = mutableListOf()

    fun addItem(item: CartItem) {
        val it = items.singleOrNull { it.title == item.title && it.condiments.hashCode() == item.condiments.hashCode() }

        if(it != null) {
            items[items.indexOf(it)].amount += item.amount
        } else {
            items.add(item)
        }
    }

    fun getCartSize(): Int {
        var cartSize = 0

        items.forEach {
            cartSize += it.amount
        }

        return cartSize
    }

    fun getTotalPrice(): Int {
        var cartPrice = 0

        items.forEach {
            cartPrice += it.price * it.amount
            if(it.condiments.isNotEmpty()) {
                it.condiments.forEach { condiment ->
                    cartPrice += condiment.price * condiment.amount
                }
            }
        }

        return cartPrice
    }

    fun getCart(): MutableList<CartItem> = items

    fun flush() {
        items.clear()
    }
}