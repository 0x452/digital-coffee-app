package com.digital.coffee.utils

import com.digital.coffee.network.data.ErrorResponse
import com.google.gson.Gson
import retrofit2.HttpException

fun HttpException.getErrorResponse(): ErrorResponse? {
    try {
        val err = Gson().fromJson(this.response()?.errorBody()?.string(), ErrorResponse::class.java)
        err.code = this.response()?.code()
        return err
    } catch (exception: Exception) { }
    return ErrorResponse(String(), 0)
}