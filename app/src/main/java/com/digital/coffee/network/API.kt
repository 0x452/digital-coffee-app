package com.digital.coffee.network

import com.digital.coffee.DigitalCoffeeApp
import com.digital.coffee.network.interfaces.CoffeeshopService
import com.digital.coffee.network.interfaces.UserService
import com.digital.coffee.utils.SharedPref
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object API {
    private val OkHttpClient = OkHttpClient()
        .newBuilder()
        .addInterceptor {
            val sharedPref = SharedPref(DigitalCoffeeApp.getContext())
            val request = it.request().newBuilder()

            if(sharedPref.getUserToken().isNotEmpty()) {
                request.header("Authorization", sharedPref.getUserToken())
            }

            it.proceed(request.build())
        }

    private val gson = GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .create()

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.digitalcoffee.app/")
        .client(OkHttpClient.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    val coffeeshops = retrofit.create(CoffeeshopService::class.java)
    val users = retrofit.create(UserService::class.java)
}