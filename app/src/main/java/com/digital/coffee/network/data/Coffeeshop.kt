package com.digital.coffee.network.data

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class CoffeeshopData(
    @SerializedName("title") @Expose var title: String,
    @SerializedName("location") @Expose val location: String,
    @SerializedName("items") @Expose val items: List<CoffeeshopItem>,
    @SerializedName("condiments") @Expose val condiments: List<Condiment>
)

data class CoffeeshopMarkersData(
    @SerializedName("markers") @Expose var markers: List<CoffeeshopMarkerData>,
)

data class CoffeeshopMarkerData(
    @SerializedName("id") @Expose var id: Int,
    @SerializedName("title") @Expose var title: String,
    @SerializedName("location") @Expose var location: String,
    @SerializedName("url") @Expose var url: String
)

@Parcelize
data class CoffeeshopItem(
    @SerializedName("title") @Expose val title: String,
    @SerializedName("price") @Expose val price: Int,
    @SerializedName("url") @Expose val url: String,
    @SerializedName("category") @Expose val category: String
) : Parcelable

data class CartItem(
    @SerializedName("title") @Expose val title: String,
    @SerializedName("price") @Expose var price: Int,
    @SerializedName("url") @Expose(serialize = false) val url: String,
    @SerializedName("amount") @Expose var amount: Int = 0,
    @SerializedName("condiments") @Expose var condiments: List<Condiment>
) {
    private fun getTotalPrice(): Int {
        var price = 0
        condiments.forEach {
            price += it.price
        }
        return price + this.price
    }

    fun incrementPrice(times: Int): Int = getTotalPrice().times(times)
}

data class Condiment(
    @SerializedName("title") @Expose val title: String,
    @SerializedName("price") @Expose val price: Int,
    @SerializedName("url") @Expose(serialize = false) var url: String,
    @SerializedName("amount") @Expose var amount: Int
)

data class CoffeeshopOrder(
    @SerializedName("order") @Expose val order: List<CartItem>,
    @SerializedName("token") @Expose val token: String
)

data class OrderData(
    @SerializedName("order_id") @Expose var order_id: Int
)

data class StatusData(
    @SerializedName("status") @Expose var status: Int
)