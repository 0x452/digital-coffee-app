package com.digital.coffee.network.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("message") @Expose var message: String,
    @SerializedName("code") @Expose var code: Int?
)