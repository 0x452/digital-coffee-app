package com.digital.coffee.network.interfaces

import com.digital.coffee.network.data.AuthorizationData
import com.digital.coffee.network.data.ProfileOrdersData
import com.digital.coffee.network.data.RegisterRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface UserService {
    @POST("/accounts/authorize")
    suspend fun authorize(@Body registerRequest: RegisterRequest): AuthorizationData

    @GET("/accounts/orders")
    suspend fun getLatestOrders(): ProfileOrdersData
}
