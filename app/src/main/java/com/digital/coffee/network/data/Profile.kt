package com.digital.coffee.network.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProfileOrdersData(
    @SerializedName("orders") @Expose var orders: List<LatestOrdersData>
)

data class LatestOrdersData(
    @SerializedName("title") @Expose var title: String,
    @SerializedName("location") @Expose var location: String,
    @SerializedName("order_id") @Expose var order_id: Int,
    @SerializedName("order") @Expose var order: List<LatestOrderData>,
    @SerializedName("created_at") @Expose  var created_at: String
)

data class LatestOrderData(
    @SerializedName("title") @Expose var title: String,
    @SerializedName("price") @Expose var price: Int,
    @SerializedName("amount") @Expose var amount: Int
)