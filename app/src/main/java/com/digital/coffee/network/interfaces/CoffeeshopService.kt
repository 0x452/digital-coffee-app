package com.digital.coffee.network.interfaces

import com.digital.coffee.network.data.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface CoffeeshopService {
    @GET("/coffeeshops/{coffeeshopId}")
    suspend fun getCoffeeshop(@Path("coffeeshopId") coffeeshopId: Int): CoffeeshopData

    @POST("/coffeeshops/{coffeeshopId}/order")
    suspend fun sendOrderToCoffeeshop(@Path("coffeeshopId") coffeeshopId: Int, @Body order: CoffeeshopOrder): OrderData

    @GET("/coffeeshops/{coffeeshopId}/order/{orderId}/status")
    suspend fun getOrderStatus(@Path("coffeeshopId") coffeeshopId: Int, @Path("orderId") orderId: Int): StatusData

    @GET("/coffeeshops/markers")
    suspend fun getCoffeeshopsMarkers(): CoffeeshopMarkersData
}