package com.digital.coffee.network.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    @SerializedName("username") @Expose var username: String,
    @SerializedName("phone") @Expose var phone: String
)

data class AuthorizationData(
    @SerializedName("token") @Expose var token: String
)