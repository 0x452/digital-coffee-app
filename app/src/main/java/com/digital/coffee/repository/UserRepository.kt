package com.digital.coffee.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.digital.coffee.network.API
import com.digital.coffee.network.data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository {
    private val _authData: MutableLiveData<AuthorizationData> = MutableLiveData()
    private val _profileData: MutableLiveData<ProfileOrdersData> = MutableLiveData()

    val authData: LiveData<AuthorizationData>
        get() = _authData
    val profileData: LiveData<ProfileOrdersData>
        get() = _profileData

    suspend fun authorize(username: String, phone: String) {
        withContext(Dispatchers.IO) {
            _authData.postValue(API.users.authorize(RegisterRequest(username, phone)))
        }
    }

    suspend fun getLatestOrders() {
        withContext(Dispatchers.IO) {
            _profileData.postValue(API.users.getLatestOrders())
        }
    }
}