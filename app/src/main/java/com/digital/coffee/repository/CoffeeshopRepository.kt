package com.digital.coffee.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.digital.coffee.network.API
import com.digital.coffee.network.data.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CoffeeshopRepository {
    private var _coffeeshop: MutableLiveData<CoffeeshopData> = MutableLiveData()
    private var _orderData: MutableLiveData<OrderData> = MutableLiveData()
    private var _status: MutableLiveData<StatusData> = MutableLiveData()
    private var _markers: MutableLiveData<CoffeeshopMarkersData> = MutableLiveData()

    val coffeeshop: LiveData<CoffeeshopData>
        get() = _coffeeshop
    val orderData: LiveData<OrderData>
        get() = _orderData
    val status: LiveData<StatusData>
        get() = _status
    val markers: LiveData<CoffeeshopMarkersData>
        get() = _markers

    suspend fun getCoffeeshop(coffeeshopId: Int) {
        withContext(Dispatchers.IO) {
            _coffeeshop.postValue(API.coffeeshops.getCoffeeshop(coffeeshopId))
        }
    }

    suspend fun sendToCoffeeshop(coffeeshopId: Int, order: CoffeeshopOrder) {
        withContext(Dispatchers.IO) {
            _orderData.postValue(API.coffeeshops.sendOrderToCoffeeshop(coffeeshopId, order))
        }
    }

    suspend fun getOrderStatus(coffeeshopId: Int, orderId: Int) {
        withContext(Dispatchers.IO) {
            _status.postValue(API.coffeeshops.getOrderStatus(coffeeshopId, orderId))
        }
    }

    suspend fun getCoffeeshopsMarkers() {
        withContext(Dispatchers.IO) {
            _markers.postValue(API.coffeeshops.getCoffeeshopsMarkers())
        }
    }
}