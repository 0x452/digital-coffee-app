package com.digital.coffee.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.digital.coffee.R
import com.digital.coffee.databinding.FragmentStatusBinding
import com.digital.coffee.viewmodels.MenuViewModel
import com.digital.coffee.viewmodels.StatusViewModel
import timber.log.Timber

class StatusFragment : Fragment() {
    private val viewModel: StatusViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, StatusViewModel.Factory(activity.application))
            .get(StatusViewModel::class.java)
    }

    private val args: StatusFragmentArgs by navArgs()

    private companion object OrderStatus {
        const val IN_PROCESS: Int = 1
        const val READY: Int = 2
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentStatusBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_status,
            container,
            false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.orderId = args.orderId
        binding.status = "Принят"
        
        binding.swipeRefreshStatus.setOnRefreshListener {
            viewModel.getCoffeeshopStatus(args.coffeeshopId, args.orderId)
            viewModel.status.observe(viewLifecycleOwner, Observer {
                when(it.status) {
                    IN_PROCESS -> binding.status = "Готовится 👨‍🍳"
                    READY -> binding.status = "Готов ☕️"
                }
                binding.swipeRefreshStatus.isRefreshing = false
            })
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback {
            findNavController().navigate(R.id.scannerFragment)
        }
    }
}