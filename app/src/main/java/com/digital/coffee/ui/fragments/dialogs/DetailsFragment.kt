package com.digital.coffee.ui.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.adapters.CondimentClick
import com.digital.coffee.adapters.CondimentsAdapter
import com.digital.coffee.databinding.FragmentDetailsBinding
import com.digital.coffee.network.data.CartItem
import com.digital.coffee.viewmodels.DetailsViewModel
import com.digital.coffee.viewmodels.SharedViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class DetailsFragment : BottomSheetDialogFragment() {
    private val viewModel: DetailsViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, DetailsViewModel.Factory(activity.application))
            .get(DetailsViewModel::class.java)
    }

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private lateinit var binding: FragmentDetailsBinding
    private lateinit var condimentsAdapter: CondimentsAdapter

    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_details,
            container,
            false
        )

        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.setDetailsItem(CartItem(args.item.title, args.item.price, args.item.url, 0, listOf()))
        condimentsAdapter = CondimentsAdapter(CondimentClick { it, position ->
            val item = viewModel.getDetailsItem()

            if(item.amount == 0) item.amount = 1

            val amount = viewModel.addCondimentInItem(it)

            condimentsAdapter.setCondimentAmount(position, amount)
            updateDetails()
        })

        condimentsAdapter.data = sharedViewModel.getCondiments().map { it.copy() }
        binding.condiments.findViewById<RecyclerView>(R.id.condiments).apply {
            layoutManager = GridLayoutManager(context, 4)
            adapter = condimentsAdapter
        }

        binding.item = viewModel.getDetailsItem()
        binding.total = 0

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.minus.setOnClickListener {
            viewModel.getDetailsItem().amount--
            if(viewModel.getDetailsItem().amount <= 0) dismiss()
            updateDetails()
        }

        binding.plus.setOnClickListener {
            viewModel.getDetailsItem().amount++
            updateDetails()
        }

        binding.add.setOnClickListener {
            if(viewModel.getDetailsItem().amount >= 1) {
                sharedViewModel.getCartManager().addItem(viewModel.getDetailsItem())
                dismissAllowingStateLoss()
            }
        }
    }

    private fun updateDetails() {
        binding.item = viewModel.getDetailsItem()
        binding.total = viewModel.getItemTotalPrice()
    }
}