package com.digital.coffee.ui

import android.app.AlertDialog
import android.content.Context

class Popup(context: Context) {
    private val dialog: AlertDialog.Builder = AlertDialog.Builder(context)

    fun showPopup(title: String, text: String) {
        dialog.setTitle(title).setMessage(text)
    }

    fun getPopup(): AlertDialog.Builder {
        return dialog
    }
}