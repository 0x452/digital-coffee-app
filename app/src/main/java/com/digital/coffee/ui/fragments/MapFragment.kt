package com.digital.coffee.ui.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.digital.coffee.R
import com.digital.coffee.adapters.ClusterAdapter
import com.digital.coffee.databinding.FragmentMapBinding
import com.digital.coffee.utils.map.MapClusterItem
import com.digital.coffee.viewmodels.MapViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager


class MapFragment : Fragment() {
    private val viewModel: MapViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, MapViewModel.Factory(activity.application))
            .get(MapViewModel::class.java)
    }

    private lateinit var googleMap: GoogleMap
    private lateinit var binding: FragmentMapBinding
    private lateinit var clusterManager: ClusterManager<MapClusterItem>
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    companion object {
        const val LOCATION_PERMISSION_REQUEST = 11
    }

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) { }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.markersData.observe(viewLifecycleOwner, { data ->
            val geo = Geocoder(requireContext())
            data?.apply {
                markers.forEach {
                    val address = geo.getFromLocationName(it.location, 2)
                    if(address.isNotEmpty()) {
                        clusterManager.addItem(
                            MapClusterItem(it.id, address[0].latitude, address[0].longitude, it.title, "", it.url)
                        )
                    }
                }

                clusterManager.cluster()
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_map,
            container,
            false
        )

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync {
            googleMap = it

            googleMap.uiSettings.isIndoorLevelPickerEnabled = false
            googleMap.uiSettings.isMapToolbarEnabled = false

            clusterManager = ClusterManager(context, googleMap)

            requestLocation()
        }

        return binding.root
    }

    private fun requestLocation() {
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 10 * 1000
        locationRequest.fastestInterval = 1 * 1000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            googleMap.isMyLocationEnabled = true

            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    location?.apply {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 15.0f))
                    }
                }

            clusterManager.setOnClusterItemClickListener {
                val action = MapFragmentDirections.actionMapFragmentToMapDetailsFragment(it)
                findNavController().navigate(action)
                true
            }

            googleMap.setOnCameraIdleListener(clusterManager)

            clusterManager.renderer = ClusterAdapter(requireContext(), googleMap, clusterManager)

            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_PERMISSION_REQUEST -> requestLocation()
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}