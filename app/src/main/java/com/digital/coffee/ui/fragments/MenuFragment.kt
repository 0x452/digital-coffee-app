package com.digital.coffee.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.adapters.ItemClick
import com.digital.coffee.adapters.MenuAdapter
import com.digital.coffee.databinding.FragmentMenuBinding
import com.digital.coffee.network.data.Errors
import com.digital.coffee.network.data.CoffeeshopData
import com.digital.coffee.ui.Popup
import com.digital.coffee.viewmodels.MenuViewModel
import com.digital.coffee.viewmodels.SharedViewModel

class MenuFragment : Fragment() {
    private val viewModel: MenuViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, MenuViewModel.Factory(activity.application, args.coffeeshopId))
            .get(MenuViewModel::class.java)
    }

    private lateinit var binding: FragmentMenuBinding

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private var viewModelAdapter: MenuAdapter? = null

    private var coffeeshopData: CoffeeshopData? = null

    private val args: MenuFragmentArgs by navArgs()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.coffeeshopData.observe(viewLifecycleOwner, { data ->
            data?.apply {
                coffeeshopData = data
                viewModelAdapter?.data = data.items
                sharedViewModel.setCondimentsList(data.condiments)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_menu,
            container,
            false)

        binding.lifecycleOwner = viewLifecycleOwner

        viewModelAdapter = MenuAdapter(ItemClick {
            val action = MenuFragmentDirections.actionMenuFragmentToDetailsFragment(it)
            findNavController().navigate(action)
        })

        binding.root.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewModelAdapter
        }

        viewModel.eventNetworkError.observe(viewLifecycleOwner, { isNetworkError ->
            if (isNetworkError) onNetworkError()
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cartButton.root.setOnClickListener {
            val action = MenuFragmentDirections.checkoutFragment(args.coffeeshopId)
            findNavController().navigate(action)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.actions, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_location -> {
                val mapIntent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q=${coffeeshopData?.location}&avoid=tf"))
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }

            R.id.action_profile -> {
                val action = MenuFragmentDirections.actionMenuFragmentToProfileFragment()
                findNavController().navigate(action)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onNetworkError() {
        if(!viewModel.isNetworkErrorShown.value!!) {
            val dialog = Popup(requireContext())

            when(viewModel.network.value?.code) {
                Errors.NotFound.code -> dialog.showPopup("Произошла ошибка", "Невозможно найти эту кофейню 🕵🏼‍♀️")
            }

            dialog.getPopup().setPositiveButton("Закрыть") { _, _ -> findNavController().navigate(R.id.scannerFragment) }.show()
        }
    }
}

