package com.digital.coffee.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.digital.coffee.R
import com.digital.coffee.adapters.CameraAdapter
import com.digital.coffee.databinding.FragmentScannerBinding

class ScannerFragment : Fragment() {
    private lateinit var binding: FragmentScannerBinding
    private lateinit var cameraAdapter: CameraAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_scanner,
            container,
            false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraAdapter = CameraAdapter(requireContext(), binding.cameraView)
        cameraAdapter.navigateToMenu.observe(viewLifecycleOwner, Observer { it ->
            it.getContentIfNotHandled()?.let {
                val action = ScannerFragmentDirections.actionScannerFragmentToMenuFragment(it)
                findNavController().navigate(action)
            }
        })

        binding.mapButton.root.setOnClickListener {
            findNavController().navigate(R.id.mapFragment)
        }
    }
}
