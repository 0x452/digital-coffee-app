package com.digital.coffee.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.adapters.CheckoutAdapter
import com.digital.coffee.databinding.FragmentCheckoutBinding
import com.digital.coffee.network.data.CoffeeshopOrder
import com.digital.coffee.utils.SharedPref
import com.digital.coffee.utils.payment.PaymentManager
import com.digital.coffee.utils.payment.PaymentsUtil
import com.digital.coffee.viewmodels.CheckoutViewModel
import com.digital.coffee.viewmodels.SharedViewModel
import com.google.android.gms.wallet.AutoResolveHelper
import com.google.android.material.snackbar.Snackbar

class CheckoutFragment : Fragment() {

    private lateinit var binding: FragmentCheckoutBinding

    private val viewModel: CheckoutViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, CheckoutViewModel.Factory(activity.application))
            .get(CheckoutViewModel::class.java)
    }

    private val sharedViewModel: SharedViewModel by activityViewModels()
    private var viewModelAdapter: CheckoutAdapter? = null

    private lateinit var paymentManager: PaymentManager
    private val navController by lazy { NavHostFragment.findNavController(this) }
    private val args: CheckoutFragmentArgs by navArgs()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModelAdapter?.data = sharedViewModel.getCartManager().getCart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        paymentManager = PaymentManager(requireActivity())

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_checkout,
            container,
            false
        )

        binding.lifecycleOwner = viewLifecycleOwner

        viewModelAdapter = CheckoutAdapter()

        binding.root.findViewById<RecyclerView>(R.id.checkout_recycler_view).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewModelAdapter
        }

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT)
        {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ) = false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModelAdapter?.removeAt(viewHolder.adapterPosition)
            }
        }).attachToRecyclerView(binding.checkoutRecyclerView)

        viewModel.eventNetworkError.observe(viewLifecycleOwner, Observer { isNetworkError ->
            if (isNetworkError) onNetworkError()
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.androidPay.root.setOnClickListener {
            if (sharedViewModel.getCartManager().getCartSize() > 0) {
                paymentManager.possiblyShowGooglePayButton()
                paymentManager.processPayment(sharedViewModel.getCartManager().getTotalPrice())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            PaymentsUtil.Constants.LOAD_PAYMENT_DATA_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val sharedPref = SharedPref(requireContext())
                        viewModel.sendOrderToCoffeeshop(
                            args.coffeeshopId, CoffeeshopOrder(
                                sharedViewModel.getCartManager().getCart(),
                                sharedPref.getToken()
                            )
                        )
                        viewModel.order.observe(viewLifecycleOwner) {
                            sharedViewModel.getCartManager().flush()
                            val action =
                                CheckoutFragmentDirections.actionCheckoutFragmentToStatusFragment(
                                    it.order_id,
                                    args.coffeeshopId
                                )
                            navController.navigate(action)
                        }
                    }

                    Activity.RESULT_CANCELED -> { }

                    AutoResolveHelper.RESULT_ERROR -> {
                        val errorCode = AutoResolveHelper.getStatusFromIntent(data)
                        Snackbar.make(
                            requireView(),
                            errorCode.toString(),
                            Snackbar.LENGTH_LONG
                        )
                    }
                }
            }
        }
    }

    private fun onNetworkError() {
        if (!viewModel.isNetworkErrorShown.value!!) {
            Snackbar.make(
                requireView(),
                "Сетевая ошибка" + viewModel.networkMessage,
                Snackbar.LENGTH_LONG
            )
        }
    }
}