package com.digital.coffee.ui.fragments.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.digital.coffee.R
import com.digital.coffee.databinding.FragmentMapDetailsBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MapDetailsFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentMapDetailsBinding
    private val args: MapDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_map_details,
            container,
            false
        )

        binding.lifecycleOwner = viewLifecycleOwner
        binding.item = args.item

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.select.setOnClickListener {
            val action =
                MapDetailsFragmentDirections.actionMapDetailsFragmentToMenuFragment(args.item.getId())
            findNavController().navigate(action)
        }
    }
}