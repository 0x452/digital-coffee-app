package com.digital.coffee.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digital.coffee.R
import com.digital.coffee.adapters.ProfileAdapter
import com.digital.coffee.databinding.FragmentProfileBinding
import com.digital.coffee.viewmodels.ProfileViewModel
import timber.log.Timber

class ProfileFragment: Fragment() {
    private val viewModel: ProfileViewModel by lazy {
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(this, ProfileViewModel.Factory(activity.application))
            .get(ProfileViewModel::class.java)
    }

    private lateinit var viewModelAdapter: ProfileAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.userProfileData.observe(viewLifecycleOwner, { data ->
            data?.apply {
                viewModelAdapter.data = data.orders
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentProfileBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_profile,
            container,
            false)

        binding.lifecycleOwner = viewLifecycleOwner

        viewModelAdapter = ProfileAdapter()

        binding.root.findViewById<RecyclerView>(R.id.latest_orders).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewModelAdapter
        }

        return binding.root
    }
}